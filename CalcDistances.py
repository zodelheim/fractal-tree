from copy import deepcopy
import os
from turtle import color

import numpy as np
import networkx as nx
import vtk
from vtk.util import numpy_support
# from graph_tool.all import *
from tqdm import tqdm
import matplotlib.pyplot as plt
import pandas as pd
from pathlib import Path
import pyvista as pv
from tqdm import tqdm
from scipy.interpolate import RBFInterpolator


if __name__ == "__main__":
    # exp_number = 1
    # fdir = "/home/ar3/Documents/PYTHON/quickeik/data/002"
    # for i in tqdm(range(1,38)):
    # for i in [1]:
    i = 3
    exp_number = 1
    # hpsystem = 'lv'
    hpsystem = 'rv'
    
    remove_rv_septum = True
    # for exp_number in tqdm(range(1,11)):
    
    # for i in tqdm(range(1, 58)):
    # for i in [3]:
    # for exp_number in tqdm(range(50, 60)):
    # for exp_number in [1]:
    for i in [21, 22, 23, 24, 28, 30, 33]:

        patient = str(i).zfill(3)
        # fdir = "/home/ar3/Documents/PYTHON/quickeik/data/Models/{}".format(str(i).zfill(3))
        # fdir = '/home/ar3/Documents/PYTHON/quickeik/data/GroupFibrosis/real_fibrosis/003'
        
        # fdir = '/home/ar3/Documents/PYTHON/fastECG/data/amiHiRes/001/' 
        # fdir = '/home/ar3/Documents/PYTHON/fractal-tree/meshes'
        # fdir = Path(f'/home/ar3/Documents/PYTHON/quickeik/data/Models/{patient}')
        # fdir = Path('/home/ar3/Documents/PYTHON/quickeik/data/Quadripole/real/056')
        
        fdir = Path(f'/home/ar3/Documents/PYTHON/quickeik/data/GroupFibrosis/{str(patient).zfill(3)}_aha_fib')
        
        # fdir = '/home/ar3/Documents/DATASETS/Almaz data/MRI 3D fibrosis/011'
        fname = f'{hpsystem}_line_{exp_number}.vtk'
        
        heart_fname = 'AHA.vtu'
        
        heart = pv.read(fdir/heart_fname)

        mesh = pv.read(fdir/'HisPurkinje'/fname)
        points = mesh.points
        cells = mesh.cells.reshape((-1, 3))[:,1:]

        ebunch = []
        for cell_id in range(mesh.GetNumberOfCells()):
            # print(cell_id)
            point_id0 = cells[cell_id][0]
            point_id1 = cells[cell_id][1]
            
            point0 = (points[point_id0])
            point1 = (points[point_id1])

            dist = np.sqrt(vtk.vtkMath.Distance2BetweenPoints(point0, point1))

            ebunch.append((point_id0, point_id1, {"weight": dist}))

        g = nx.Graph()
        g.add_edges_from(ebunch)
        
        # print(len(g.nodes))
        # print(len(g.edges))
        # print(len(points))
        # print(len(cells))
        
        # exit()
        # -------------------------- anter or poster blockade modeling----------------------------
        
        # if hpsystem == 'lv': 
        #     branching_node = (np.argwhere(np.array(g.degree())[:, 1] > 2.5)[0])[0]
        #     root_edge, anter_edge, poster_edge = g.edges(branching_node)
        #     root_node, anter_node, poster_node = root_edge[1], anter_edge[1], poster_edge[1]

        #     remove_branch = anter_node
        #     g.remove_node(remove_branch)
        #     con_nodes = nx.node_connected_component(g, remove_branch+1)
        #     g.remove_nodes_from(list(con_nodes))

        # ---------------------------------------------------------------------------------------
        # #* interpolate
        
        # x = heart.points
        # y = heart['apex_base']
        # x_new = mesh.points
        
        # y_new = RBFInterpolator(x, y)(x_new) 
        
        # mesh['apex_base'] = y_new
                
        # mesh_distal = deepcopy(mesh)
        # mesh_distal = mesh_distal.threshold((-0.5, 0.5), 'apex_base', 'point')
        
        # mesh_distal = mesh_distal.connectivity()
        # index = mesh_distal.get_array('RegionId', preference='point')[0]
        # index = 0
        # mesh_distal.plot(scalars='RegionId')
        # mesh_distal = mesh_distal.threshold((index-0.1, index+0.1), 'RegionId', preference='point')
        # # 

        # plotter = pv.Plotter()
        # plotter.add_mesh(mesh_distal, scalars='apex_base', point_size=10)
        # plotter.add_mesh(mesh_distal.points[0], render_points_as_spheres=True,  point_size=10)
        # plotter.add_mesh(mesh.points[0], render_points_as_spheres=True, point_size=20)
        # plotter.add_mesh(mesh.points[1], render_points_as_spheres=True, point_size=20)
        # plotter.add_mesh(mesh.points[2], render_points_as_spheres=True, point_size=20)
        # plotter.show()
        
        # exit() 
        # ------------------------------------------------------------------------------------------
        #* remove RV septum
        if remove_rv_septum:
            
            leaves = [node for node in g.nodes() if len(g.edges(node)) == 1]
            root_node = 0
            
            length_to_nodes = []
            
            length_dict = nx.single_source_dijkstra(g, root_node)[0]
            length_to_nodes = list(length_dict.values())
            length_to_nodes = np.array(length_to_nodes)
            
            mesh['Length'] = length_to_nodes[:-1]
            mesh = mesh.ctp()
            
            x = heart.points
            print(heart.array_names)
            try:
                y = heart['LV/RV']
            except KeyError:
                y = heart['LV colors']
            
            x_new = mesh.points
            
            y_new = RBFInterpolator(x, y)(x_new) 
            
            mesh['LV/RV'] = y_new
            
            # mesh.plot(scalars='LV/RV')
            # mesh.plot(scalars='Length')
            
            septal_edges = np.argwhere((mesh['LV/RV'] > 0.5)&(mesh['Length'] > 120)).ravel()
            g.remove_nodes_from(septal_edges.tolist())
            
            # largest_cc = max(nx.connected_components(g), key=len)
            
            if len(list(nx.connected_components(g))) > 1:
                for component in list(nx.connected_components(g))[1:]:
                    g.remove_nodes_from(component)

            plotter = pv.Plotter()
            # plotter.add_mesh(mesh_distal, scalars='apex_base', point_size=10)
            plotter.add_mesh(mesh, scalars='LV/RV', line_width=10)
            plotter.add_mesh(mesh.points[g.nodes], render_points_as_spheres=True)
            plotter.show()
            # g = g.remove_nodes_from(np.argwhere(length_to_nodes > 120))

        #-------------------------------------------------------------------------------------------
        leaves = [node for node in g.nodes() if len(g.edges(node)) == 1]

        speed = 3.7

        paths = np.array([nx.dijkstra_path_length(g, 0, leaf) for leaf in leaves])/speed

        dframe = pd.DataFrame(data=points[leaves], columns=['x', 'y', 'z'])
        dframe['LAT'] = paths
        # print(dframe)

        if remove_rv_septum:
            dframe.to_csv(fdir/'HisPurkinje'/f'purk_{hpsystem}_{exp_number}_noseptum.csv')
        else:
            dframe.to_csv(fdir/'HisPurkinje'/f'purk_{hpsystem}_{exp_number}.csv')
        
        
        plotter = pv.Plotter()
        plotter.add_mesh(pv.read(fdir/heart_fname), color='white')
        plotter.add_mesh(mesh, line_width=10, color='black')
        plotter.add_mesh(points[leaves], color='blue', render_points_as_spheres=True, point_size=10)
        plotter.show()
        # exit()


        # dframe["LAT"].hist()
        # print(dframe["LAT"].max())
        # print(dframe["LAT"].mean())
        # plt.show()

        # print(dframe)
        # exit()
        # print(paths)
        # print(points[leaves])
        # print(leaves)
