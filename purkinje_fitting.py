import numpy as np
import vtk
from vtk.util import numpy_support
import os

def read_XMLUGrid(filedir, filename):
    reader = vtk.vtkXMLUnstructuredGridReader()
    reader.SetFileName(os.path.join(filedir, filename))
    reader.Update()
    return reader.GetOutput()

def write_XMLUGrid(filedir, filename, data):
    # data = make_UGrid(data)
    writer = vtk.vtkXMLUnstructuredGridWriter()
    writer.SetFileName(os.path.join(filedir, filename))
    writer.SetInputData(data)
    writer.Write()

if __name__ == "__main__":
    filedir = '/home/ar3/Documents/PYTHON/quickeik/data/028'
    filename = 'heart.s.vtu'
    rvtree_filename = 'rv-line.vtu'
    lvtree_filename = 'lv-line.vtu'

    mesh = read_XMLUGrid(filedir, filename)
    rv_tree = read_XMLUGrid(filedir, rvtree_filename)
    rv_tree_points = numpy_support.vtk_to_numpy(rv_tree.GetPoints().GetData())
    lv_tree = read_XMLUGrid(filedir, lvtree_filename)
    lv_tree_points = numpy_support.vtk_to_numpy(lv_tree.GetPoints().GetData())

    kdtree = vtk.vtkKdTreePointLocator()
    kdtree.SetDataSet(mesh)
    kdtree.BuildLocator()

    lv_points_ind = np.array([kdtree.FindClosestPoint(lv_tree_points[i]) for i in xrange(len(lv_tree_points))],dtype=int)
    rv_points_ind = np.array([kdtree.FindClosestPoint(rv_tree_points[i]) for i in xrange(len(rv_tree_points))],dtype=int)

    scalars = np.zeros(mesh.GetNumberOfPoints())

    for i in lv_points_ind:
        scalars[i] = 10
    for i in rv_points_ind:
        scalars[i] = 10

    scalars_vtk = numpy_support.numpy_to_vtk(
            scalars.ravel(), deep=True, array_type=vtk.VTK_FLOAT)
    scalars_vtk.SetName('Purkinje')

    mesh.GetPointData().AddArray(scalars_vtk)

    write_XMLUGrid(filedir, 'heart_purk.vtu', mesh)






