from vedo import *
from pathlib import Path
import numpy as np
import vtk 
import networkx as nx
from tqdm import tqdm
import pyvista as pv 


fdir = Path('/home/ar3/Documents/PYTHON/fractal-tree/meshes')
fname = 'heart.s.vtk'

mesh = Mesh((fdir/fname).as_posix())

lv_branch = pv.read(fdir/'lv_line_0.vtu')
rv_branch = pv.read(fdir/'rv_line_0.vtu')

av_node = Sphere(pos=mesh.points()[5780], r=5)
his_node = Sphere(pos=mesh.points()[5952], r=3)

print(mesh.points()[5061],)

his_branch = Spline(
    points=[
        mesh.points()[5780], # av node
        mesh.points()[5952], # his
        # [31.339, -126.72, 24.55],
        
        # mesh.points()[5061],
        [28.09, -138.753, 45.322],

        
        mesh.points()[3094], #rv
        mesh.points()[6191], #rv
        mesh.points()[6189], #lv
        ],
    smooth=0,
    degree=2,
    res=1000
)


ebunch = []
for cell_id in tqdm(range(lv_branch.GetNumberOfCells())):

    point_id0 = lv_branch.GetCell(cell_id).GetPointId(0)
    point_id1 = lv_branch.GetCell(cell_id).GetPointId(1)
    point0 = (lv_branch.points[point_id0])
    point1 = (lv_branch.points[point_id1])

    dist = np.sqrt(vtk.vtkMath.Distance2BetweenPoints(point0, point1))

    ebunch.append((point_id0, point_id1, {"weight": dist}))

g_lv = nx.Graph()
g_lv.add_edges_from(ebunch)

paths_lv = np.array([nx.shortest_path_length(g_lv, 0, leaf) for leaf in tqdm(range(lv_branch.n_points))])

ebunch = []
for cell_id in tqdm(range(rv_branch.GetNumberOfCells())):

    point_id0 = rv_branch.GetCell(cell_id).GetPointId(0)
    point_id1 = rv_branch.GetCell(cell_id).GetPointId(1)
    point0 = (rv_branch.points[point_id0])
    point1 = (rv_branch.points[point_id1])

    dist = np.sqrt(vtk.vtkMath.Distance2BetweenPoints(point0, point1))

    ebunch.append((point_id0, point_id1, {"weight": dist}))

g_rv = nx.Graph()
g_rv.add_edges_from(ebunch)

paths_rv = np.array([nx.shortest_path_length(g_rv, 0, leaf) for leaf in tqdm(range(rv_branch.n_points))])

lbb = Spheres(lv_branch.points, r=2*(np.max(paths_lv) - paths_lv)/np.max((np.max(paths_lv) - paths_lv)))
rbb = Spheres(rv_branch.points, r=2*(np.max(paths_rv) - paths_rv)/np.max((np.max(paths_rv) - paths_rv)))

lbb_zeronode = 0
lbb_branching = 65
lbb_anternode = 348
lbb_posternode = 1632
rbb_zeronode = 0
rbb_lastnode = 179

# lbb_anterior = lv_branch.extract_geometry().geodesic(lbb_zeronode, lbb_anternode)
# lbb_posterior = lv_branch.extract_geometry().geodesic(lbb_zeronode, lbb_posternode)
# rbb_branch = rv_branch.extract_geometry().geodesic(rbb_zeronode, rbb_lastnode)


lbb_branch_nodes =  nx.shortest_path(g_lv, lbb_zeronode, lbb_branching)
lbb_anter_nodes =  nx.shortest_path(g_lv, lbb_branching, lbb_anternode)
lbb_poster_nodes =  nx.shortest_path(g_lv, lbb_branching, lbb_posternode)

rbb_branch_nodes =  nx.shortest_path(g_rv, rbb_zeronode, rbb_lastnode)

# lbb_nodes = np.array(list(lbb_anter_nodes.union(lbb_poster_nodes)))
# rbb_nodes = np.array(rbb_branch_nodes)

lbb_branch = Spline(lv_branch.points[lbb_branch_nodes], degree=1, smooth=0)
lbb_anter = Spline(lv_branch.points[lbb_anter_nodes], degree=1, smooth=0)
lbb_poster = Spline(lv_branch.points[lbb_poster_nodes], degree=1, smooth=0)

# lbb_branch = lbb_branch + lbb_anter + lbb_poster

lbb_branch = merge(lbb_branch, lbb_anter, lbb_poster)
rbb_branch =  Spline(rv_branch.points[rbb_branch_nodes], degree=1, smooth=0)



show(
    mesh.color('white').opacity(0.8),
    av_node.color('red'),
    his_node.color('blue'),
    his_branch.color('green').pointSize(20),
    # rv_branch.color('black').pointSize(5),
    # lv_branch.color('black').pointSize(5),
    # Mesh(lbb_anterior.extract_geometry()).color('black').pointSize(20),
    # Mesh(lbb_posterior.extract_geometry()).color('black').pointSize(20),
    # Mesh(rbb_branch.extract_geometry()).color('black').pointSize(20),
    # lbb.color('black'),
    # rbb.color('black'),
    lbb_branch.color('black').pointSize(20),
    rbb_branch.color('black').pointSize(20),
    interactive=True,
).close()


mesh.write((fdir/'heart.vtk').as_posix())

av_node.write((fdir/'av_node.vtk').as_posix())
his_node.write((fdir/'his_node.vtk').as_posix())
his_branch.write((fdir/'his_branch.vtk').as_posix())

lbb_branch.write((fdir/'lbb_branch.vtk').as_posix())
rbb_branch.write((fdir/'rbb_branch.vtk').as_posix())

lbb.write((fdir/'lbb_branch_and_purkinje.vtk').as_posix())
rbb.write((fdir/'rbb_branch_and_purkinje.vtk').as_posix())




# mesh.write((fdir/'heart.vtu').as_posix())
