# -*- coding: utf-8 -*-
import os

import numpy as np 
import networkx as nx
import vtk
from vtk.util import numpy_support
# from graph_tool.all import *
from tqdm import tqdm
import matplotlib.pyplot as plt
import pandas as pd

if __name__ == "__main__":

    fdir = "/home/ar3/Documents/DATASETS/Almaz data/Stepan2018/Акулов_endo_ventr_isoRVbi_ecg2_3_LbQaUT"
    fname = "heart.vtk"

    reader = vtk.vtkPolyDataReader()
    reader.SetFileName(os.path.join(fdir, fname))
    reader.Update()

    mesh = reader.GetOutput()
    points = numpy_support.vtk_to_numpy(mesh.GetPoints().GetData())
    cells = numpy_support.vtk_to_numpy(mesh.GetPolys().GetData())

    print(cells)