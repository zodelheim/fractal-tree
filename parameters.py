# -*- coding: utf-8 -*-
"""
This module contains the Parameters class that is used to specify the input parameters of the tree.
"""

import os

import numpy as np
from math import pi
import vtk
from sklearn.neighbors import KDTree
import pyvista as pv
from pathlib import Path
class Parameters():
    """Class to specify the parameters of the fractal tree.

    Attributes:
        meshfile (str): path and filename to obj file name.
        filename (str): name of the output files.
        init_node (numpy array): the first node of the tree.
        second_node (numpy array): this point is only used to calculate the initial direction of the tree and is not included in the tree. Please avoid selecting nodes that are connected to the init_node by a single edge in the mesh, because it causes numerical issues.
        init_length (float): length of the first branch.
        N_it (int): number of generations of branches.
        length (float): average lenght of the branches in the tree.
        std_length (float): standard deviation of the length. Set to zero to avoid random lengths.
        min_length (float): minimum length of the branches. To avoid randomly generated negative lengths.
        branch_angle (float): angle with respect to the direction of the previous branch and the new branch.
        w (float): repulsivity parameter.
        l_segment (float): length of the segments that compose one branch (approximately, because the lenght of the branch is random). It can be interpreted as the element length in a finite element mesh.
        Fascicles (bool): include one or more straigth branches with different lengths and angles from the initial branch. It is motivated by the fascicles of the left ventricle.
        fascicles_angles (list): angles with respect to the initial branches of the fascicles. Include one per fascicle to include.
        fascicles_length (list): length  of the fascicles. Include one per fascicle to include. The size must match the size of fascicles_angles.
        save (bool): save text files containing the nodes, the connectivity and end nodes of the tree.
        save_paraview (bool): save a .vtu paraview file. The tvtk module must be installed.

    """
    def __init__(self):
        # self.meshfile='sphere.obj'
        self.meshfile='endo.obj'
        self.filename='lv_line'

        self.init_node = np.array([24,	-192,	-191]) # lv
        # self.second_node = np.array([97, -221,	-228]) # lv
        self.second_node = np.array([91, -209,	-234]) # lv

        # self.init_node = np.array([17, -201, -195])     #rv
        # self.second_node = np.array([54, -233, -227 ])  #rv

        self.init_length=20
        # self.init_length=40 #rv
#Number of iterations (generations of branches)
        self.N_it = 15
        # self.N_it = 11 # rv
        #Median length of the branches
        self.length = 7
        #Standard deviation of the length
        self.std_length = np.sqrt(0.1)*self.length
#Min length to avoid negative length
        self.min_length = self.length/10
        self.branch_angle=0.15
        self.w=0.1
#Length of the segments (approximately, because the lenght of the branch is random)
        self.l_segment = 0.6

        self.Fascicles=True
###########################################
# Fascicles data
###########################################
        self.fascicles_angles=[-pi/2, pi/8] #rad
        # self.fascicles_length=[.5,.5]
        self.fascicles_length=[20, 10]
# Save data?
        self.save=True
        self.save_paraview=True

def visualize(mesh):
    uGrid_to_polydata = vtk.vtkGeometryFilter()
    uGrid_to_polydata.SetInputData(mesh)
    uGrid_to_polydata.Update()
    mesh = uGrid_to_polydata.GetOutput()

    mapper = vtk.vtkPolyDataMapper()
    mapper.SetInputData(mesh)
    mapper.ScalarVisibilityOn()

    actor = vtk.vtkActor()
    actor.SetMapper(mapper)
    actor.GetProperty().EdgeVisibilityOn()

    return actor


def randomize(loc, scale=0.1):
    return gauss(loc, scale)

if __name__ == "__main__":
    from FractalTree import *
    from parameters import Parameters
    from random import gauss

    # fdir = '/home/ar3/Documents/PYTHON/quickeik/data/001/'

    # branch = 'lv'
    branch = 'rv'
    # branch = 'both'
    # branch = 'no'
    exp_number = 1
    # i = 1
    
    # for exp_number in range(50, 60):
    # for exp_number in [1]:
    # for patient in [21, 22, 23, 24, 28, 30, 33]:
    # for patient in range(11, 21):
    for patient in [5, 6, 10, 12, 14, 18]:

        # for i in xrange(1,38):
        # for i in range(1,11)::
        # fdir = "/home/ar3/Documents/PYTHON/quickeik/data/{}".format(str(i).zfill(3))
        # fdir = '/home/ar3/Documents/DATASETS/Almaz data/MRI 3D fibrosis/011'
        # fdir = '/home/ar3/Documents/PYTHON/quickeik/data/GroupFibrosis/real_fibrosis/003'
        # fdir = '/home/ar3/Documents/PYTHON/fastECG/data/ami/001/' 
        # fdir = '/home/ar3/Documents/PYTHON/fractal-tree/meshes/'
        
        # fdir = Path('/home/ar3/Documents/PYTHON/quickeik/data/Models/003')
        # fdir = Path('/home/ar3/Documents/PYTHON/quickeik/data/Quadripole/real/056')
        fdir = Path(f'/home/ar3/Documents/PYTHON/quickeik/data/GroupFibrosis/{str(patient).zfill(3)}_aha_fib')
        if not fdir.exists():
            continue
        print(fdir)

        # mesh = pv.read(fdir/'heart.v.vtu')
        mesh = pv.read(fdir/'AHA.vtu')
        mesh = mesh.extract_surface()
        # mesh = mesh.ctp()
        points = mesh.points
        # mesh.plot()

        # circ = numpy_support.vtk_to_numpy(mesh.GetPointData().GetArray('circle'))
        try:
            circ = mesh['Circle']
        except KeyError:
            circ = mesh['circle']

        try:
            apex_base = mesh['apex_base']
        except KeyError:
            apex_base = mesh['Apex_base']
        try:
            lv_color = mesh['LDBR']
        except KeyError:
            lv_color = mesh['For LDBR']
            
        try:
            transv = mesh['transmural']
        except KeyError:
            transv = mesh['Transversal']
    
        # lv_color = numpy_support.vtk_to_numpy(mesh.GetPointData().GetArray('LDBR'))
        # transv = numpy_support.vtk_to_numpy(mesh.GetPointData().GetArray('transmural'))
        
        # points = numpy_support.vtk_to_numpy(mesh.GetPoints().GetData())
        points = mesh.points

        X1 = np.array([circ, apex_base, lv_color, transv]).T
        X2 = np.array([apex_base, lv_color, transv]).T
        tree1 = KDTree(X1, leaf_size=2)
        tree2 = KDTree(X2, leaf_size=2)

        _, id_lv_init = tree1.query([[0.01, 0.95, 3, 0]], k=1)
        _, id_rv_init = tree1.query([[0.01, 0.95, 6, 1]], k=1)   # was 0.01
        _, id_lv_second = tree2.query([[0.0, 3, 0]], k=1)
        _, id_rv_second = tree2.query([[0.1, 6, 1]], k=1)

        # id_rv_init = tree.query([[0.0, 0.95, 0]], k=1)

        id_lv_init = id_lv_init[0][0]
        id_rv_init = id_rv_init[0][0]
        id_lv_second = id_lv_second[0][0]
        id_rv_second = id_rv_second[0][0]
        
        # plotter = pv.Plotter()
        # plotter.add_mesh(mesh, color='white')
        # plotter.add_mesh(points[id_lv_init], render_points_as_spheres=True, color='black', point_size=20)
        # plotter.add_mesh(points[id_rv_init], render_points_as_spheres=True, color='black', point_size=20)
        # plotter.add_mesh(points[id_lv_second], render_points_as_spheres=True, color='green', point_size=20)
        # plotter.add_mesh(points[id_rv_second], render_points_as_spheres=True, color='green', point_size=20)
        # plotter.show()
        # exit()
        
        # print(np.linalg.norm(points[3098]-points[100]))
        # print(np.linalg.norm(points[6189]-points[4747]))
        # exit()        
            
        if not (fdir/'HisPurkinje').exists():
            (fdir/'HisPurkinje').mkdir()
    

        if branch == 'lv' or branch == 'both':
            param_lv = Parameters()
            # param_lv.init_node = np.array([34, -220, -625])
            # param_lv.second_node = np.array([119, -248, -657])
            param_lv.init_node = points[id_lv_init]
            param_lv.second_node = points[id_lv_second]
            # param_lv.init_node = points[6189]
            # param_lv.second_node = points[4747]

            # param_lv.init_length = randomize(20, 20*0.1)
            param_lv.init_length = 40
            param_lv.N_it = 15
            param_lv.N_it = 13
            # param_lv.N_it = 5
            param_lv.length = randomize(8, 8*0.1)
            param_lv.Fascicles=True
            # param_lv.fascicles_angles = [-pi/2, pi/8]
            param_lv.fascicles_angles = [-pi/3, pi/12]
            # param_lv.fascicles_length = [10, 30]    #was 40, 30
            param_lv.fascicles_length = [30, 50]    #was 40, 30
            # param_lv.meshfile = os.path.join(fdir, 'lv_surface.obj')
            
            if not (fdir/'HisPurkinje').exists():
                (fdir/'HisPurkinje').mkdir()
            
            lv_mesh = mesh.threshold((3-0.5, 3+0.5), 'LV/RV')
            lv_mesh = lv_mesh.threshold((3-0.5, 3+0.5), 'For LDBR')
            lv_endo = lv_mesh.threshold((-0.5, 0.5), 'Transversal')
            lv_endo.save(fdir/'HisPurkinje'/'lv_endo.vtk')
            param_lv.meshfile = fdir/'HisPurkinje'/'lv_endo.vtk'
            
            param_lv.filename = fdir/'HisPurkinje'/f'lv_line_{exp_number}'
            branches, nodes = Fractal_Tree_3D(param_lv)
            
            print('bazzinga')

        if branch == 'rv' or branch == 'both':
            param_rv = Parameters()
            # param_rv.init_node = np.array([29, -237, -632])
            # param_rv.second_node = np.array([89, -269, -649])
            param_rv.init_node = points[id_rv_init]
            param_rv.second_node = points[id_rv_second]
            # param_rv.init_node = points[3094]
            # param_rv.second_node = points[100]
            # param_rv.init_length = 50
            param_rv.init_length = 60
            param_rv.N_it = 19 # 15, 13, 10, 5, 19
            # param_rv.N_it = 19
            # param_rv.N_it = 5
            param_rv.length = randomize(8, 8*0.1)
            param_rv.Fascicles=False
            param_rv.branch_angle = 0.15
            # param_rv.meshfile = os.path.join(fdir, 'rv_surface.obj')

            # rv_mesh = mesh.threshold((0-0.5, 0+0.5), 'LV/RV')
            rv_endo = mesh.threshold((6-0.5, 6+0.5), 'For LDBR')
            # rv_endo = rv_mesh.threshold((-0.5, 0.5), 'transmural')
            rv_endo.save(fdir/'HisPurkinje'/'rv_endo.vtk')
            
            param_rv.meshfile = fdir/'HisPurkinje'/'rv_endo.vtk'
            
            if not (fdir/'HisPurkinje').exists():
                (fdir/'HisPurkinje').mkdir()
            param_rv.filename = fdir/'HisPurkinje'/f'rv_line_{exp_number}'
            branches, nodes = Fractal_Tree_3D(param_rv)
            
        # if branch == 'no':
            heart = pv.read(fdir/'AHA.vtu')
            # lv_line = pv.read(fdir/'HisPurkinje'/f'lv_line_{exp_number}.vtk')
            rv_line = pv.read(fdir/'HisPurkinje'/f'rv_line_{exp_number}.vtk')
            
            plotter = pv.Plotter()
            plotter.add_mesh(heart, color='white')
            # plotter.add_mesh(lv_line, color='black', line_width=5)
            plotter.add_mesh(rv_line, color='black', line_width=5)
            plotter.add_title(str(patient).zfill(3))
            plotter.show(interactive=True)
            plotter.close()


	# param_lv = Parameters()
    #     # param_lv.init_node = np.array([34, -220, -625])
    #     # param_lv.second_node = np.array([119, -248, -657])
    #     param_lv.init_node = np.array([41, -214, -175])
    #     param_lv.second_node = np.array([93, -253, -193])
    #     param_lv.init_length = 30
    #     param_lv.N_it = 15
    #     param_lv.length = 8
    #     param_lv.Fascicles=True
    #     param_lv.fascicles_angles = [-pi/2 + pi/10, pi/8]
    #     param_lv.fascicles_length = [30, 40]
    #     param_lv.meshfile = os.path.join(fdir, 'lv_surface.obj')
    #     param_lv.filename = os.path.join(fdir, 'lv_line')
	# branches, nodes = Fractal_Tree_3D(param_lv)


	# param_rv = Parameters()
    #     # param_rv.init_node = np.array([29, -237, -632])
    #     # param_rv.second_node = np.array([89, -269, -649])
    #     param_rv.init_node = np.array([41, -214, -175])
    #     param_rv.second_node = np.array([95, -254, -179])
    #     param_rv.init_length=40
    #     param_rv.N_it = 15
    #     param_rv.length = 8
    #     param_rv.Fascicles=False
    #     param_rv.branch_angle = 0.15
    #     param_rv.meshfile = os.path.join(fdir, 'rv_surface.obj')
    #     param_rv.filename = os.path.join(fdir, 'rv_line')
	# branches, nodes = Fractal_Tree_3D(param_rv)